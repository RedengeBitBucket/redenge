<?php

namespace Redenge\Money\CNB;

use Nette\Caching\Cache;
use Nette\Caching\IStorage;
use Nette\Caching\Storages\FileStorage;
use Nette\Utils\Strings;

/**
 * Exchange rates from CZK to foreign currencies by ČNB
 */
class ExchangeRate implements IExchangeRate
{
	const   API_URL = 'http://www.cnb.cz/cs/financni_trhy/devizovy_trh/kurzy_devizoveho_trhu/denni_kurz.txt?date=',
			CACHE_NAMESPACE = 'cnbExchangeRate',
			CACHE_EXPIRATION = '2 hours',
			CACHE_KEY = 'currencies',
			FILE_STORAGE_DIR = 'temp';

	/** @var Cache|NULL */
	private $cache;

	/** @var string */
	private $expiration;

	/** @var string */
	private $dataUrl;

	/** @var string Pure data from API */
	private $data;

	/** @var array|NULL */
	private $currencies;

	/**
	 * @param IStorage    $storage
	 * @param string      $expiration
	 *
	 * @return void
	 */
	public function __construct($storage = NULL, $expiration = self::CACHE_EXPIRATION)
	{
		if ($storage === NULL) {
			$storage = new FileStorage(sprintf('%s/%s', APP_DIR, self::FILE_STORAGE_DIR));
		}

		$this->expiration = $expiration;
		$this->cache = new Cache($storage, self::CACHE_NAMESPACE);
		$this->dataUrl = sprintf('%s=%s', self::API_URL, (new \DateTime())->format('d.m.Y'));
	}

	/**
	 * Zjištění kurzu
	 *
	 * @param string    $code
	 *
	 * @return float
	 * @throws \Exception\NoCurrenciesException
	 * @throws \Exception\NotFoundException
	 */
	public function get($code)
	{
		$code = Strings::upper($code);
		if (empty($this->currencies)) {
			$this->currencies = $this->getCurrencies();
		}

		if (empty($this->currencies)) {
			throw new Exception\NoCurrenciesException('Service may be unavailable');
		}

		if (!isset($this->currencies[$code])) {
			throw new Exception\NotFoundException(sprintf('Currency "%s" not available, %d currencies listed', $code, count($this->currencies)));
		}

		return $this->currencies[$code]->getRate();
	}

	/**
	 * Get "pure" data from API
	 *
	 * @return string Všechna stažená data
	 */
	public function getAllData()
	{
		return $this->getData();
	}

	/**
	 * Parse data from service or return them from cache
	 *
	 * @return array
	 */
	private function getCurrencies()
	{
		/**
		 * Vrátit z cache
		 */
		$result = $this->cache->load(self::CACHE_KEY);
		if ($result === NULL || empty($result)) {
			$result = $this->cache->save(self::CACHE_KEY, function() {
				$currencies = [];
				$data = explode("\n", $this->getData());
				/**
				 * Remove header lines (date & column names)
				 * země|měna|množství|kód|kurz
				 */
				unset($data[0], $data[1]);
				foreach ($data as $row) {
					$row = Strings::trim($row);
					if (empty($row)) {
						continue; // Due last empty row
					}
					list(, , $amount, $code, $rate) = explode('|', $row);
					$currencies[$code] = new Currency($code, floatval(Strings::replace($rate, '[,]', '.')) / (int) $amount);
				}

				return $currencies;

			}, [Cache::EXPIRE => $this->expiration]);
		}

		return $result;
	}

	/**
	 * Get data from API
	 *
	 * @return string|FALSE Data ČNB
	 */
	private function getData()
	{
		if ($this->data === NULL) {
			if (!function_exists('curl_init')) {
				$data = @file_get_contents($this->dataUrl);
			} else {
				$ch = curl_init($this->dataUrl);
				@curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
				@curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
				@curl_setopt($ch, CURLOPT_AUTOREFERER, TRUE);
				@curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				@curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:27.0) Gecko/20100101 Firefox/27.0');
				@curl_setopt($ch, CURLOPT_TIMEOUT, 30);
				$data = @curl_exec($ch);
				curl_close ($ch);
				if ($data === FALSE) {
					$data = @file_get_contents($this->dataUrl);
				}
			}

			$this->data = $data;
		}

		return $this->data;
	}
}


interface IExchangeRate
{
	/**
	 * @param string    $code
	 *
	 * @return float
	 */
	function get($code);
}
