<?php

namespace Redenge\Money\CNB;

use Nette\Caching\Cache;
use Nette\Caching\IStorage;

class ExchangeRateFactory implements IExchangeRateFactory
{
	/**
	 * @param IStorage    $storage
	 *
	 * @return ExchangeRate
	 */
	public static function create(IStorage $storage = NULL)
	{
		return new ExchangeRate($storage);
	}
}


interface IExchangeRateFactory
{
	/**
	 * @param IStorage    $storage
	 *
	 * @return ExchangeRate
	 */
	static function create(IStorage $storage = NULL);
}
